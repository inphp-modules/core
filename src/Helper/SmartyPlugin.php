<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Licensed ( https://opensource.org/licenses/MIT )
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// +----------------------------------------------------------------------
// | smarty 插件
// +----------------------------------------------------------------------
namespace Inphp\Core\Helper;

class SmartyPlugin
{
    /**
     * 所有插件
     * @var array[]
     */
    public static array $list = [
        "function"  => [],
        "block"     => [],
        "modifier"  => []
    ];

    /**
     * @param string $name
     * @param mixed $fn
     * @return void
     */
    public static function registerFunction(string $name, mixed $fn): void
    {
        self::$list["function"][$name] = $fn;
    }

    /**
     * @param string $name
     * @param mixed $fn
     * @return void
     */
    public static function registerBlock(string $name, mixed $fn): void
    {
        self::$list["block"][$name] = $fn;
    }

    /**
     * @param string $name
     * @param mixed $fn
     * @return void
     */
    public static function registerModifier(string $name, mixed $fn): void
    {
        self::$list["modifier"][$name] = $fn;
    }
}