<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Licensed ( https://opensource.org/licenses/MIT )
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// +----------------------------------------------------------------------
// | cookie 操作
// +----------------------------------------------------------------------
namespace Inphp\Core\Services\Http;

use Inphp\Core\Context;

class Cookie
{
    /**
     * 保存cookie
     * @param string $name
     * @param mixed $value
     * @param int $seconds
     */
    public static function set(string $name, mixed $value, int $seconds = 0): void
    {
        Context::getClient()->setCookie($name, $value, $seconds);
    }

    /**
     * 获取Cookie
     * @param string $name
     * @param mixed|null $default
     * @return mixed
     */
    public static function get(string $name, mixed $default = null): mixed
    {
        return Context::getClient()->getCookie($name) ?? $default;
    }

    /**
     * 移除cookie
     * @param string $name
     */
    public static function delete(string $name): void
    {
        Context::getClient()->deleteCookie($name);
    }
}