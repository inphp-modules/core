<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Licensed ( https://opensource.org/licenses/MIT )
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// +----------------------------------------------------------------------
// | 中间件接口
// +----------------------------------------------------------------------
namespace Inphp\Core\Middlewares;

interface IMiddleware
{
    public static function process(...$args);
}