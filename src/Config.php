<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Licensed ( https://opensource.org/licenses/MIT )
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// +----------------------------------------------------------------------
// | 配置
// +----------------------------------------------------------------------
namespace Inphp\Core;

use Inphp\Core\Util\Arr;
use Inphp\Core\Util\File;

class Config
{
    /**
     * 所有配置项
     * @var array
     */
    public static array $configs = [];

    /**
     * 保存配置
     * @param string $path
     * @param mixed $value
     */
    public static function set(string $path, mixed $value): void
    {
        Arr::set(self::$configs, $path, $value);
    }

    /**
     * 获取配置
     * @param string $path
     * @param mixed $default
     * @return mixed
     */
    public static function get(string $path, mixed $default = null): mixed
    {
        $path = str_replace("/", ".", $path);
        $pathArray = explode(".", $path);
        $config = self::$configs;
        $value = null;
        $configsDir = null;
        $fileSrc = [];
        foreach ($pathArray as $p) {
            if (empty($p)) {
                //空字符，略过
                continue;
            }
            $fileSrc[] = $p;
            if (is_array($config)) {
                if (!isset($config[$p])) {
                    //从文件自动加载
                    if (is_null($configsDir)) {
                        $configsDir = self::$configs["define"]["configs"];
                        if (empty($configsDir) || !is_dir($configsDir)) {
                            //文件夹不存在，不再执行
                            return $default;
                        }
                    }
                    $file = $configsDir."/".join("/", $fileSrc);
                    //2023.8.24 支持配置文件是JSON文件
                    if (is_file($file.".php") || is_file($file.".json")) {
                        //如果是文件
                        $value = is_file($file.".json") ? file_get_contents($file.".json") : (include $file.".php");
                        $value = is_string($value) ? (@json_decode($value, true) ?? []) : (is_array($value) ? $value : []);
                    } elseif (is_dir($file)) {
                        //如果是文件夹
                        $value = [];
                    } else {
                        //啥也不是
                        return $default;
                    }
                    //保存到全局配置
                    self::set(join('.', $fileSrc), $value);
                    //值
                    $config = $value;
                } else {
                    $value = $config[$p];
                    $config = $config[$p];
                }
            } else {
                return $default;
            }
        }
        return $value ?? $default;
    }

    /**
     * 保存配置文件
     * @param string $file
     * @param string|array $content
     * @return bool
     */
    public static function save(string $file, string|array $content): bool
    {
        $configsDir = self::get("define.configs");
        $file = stripos($file, "/") === 0 ? substr($file, 1) : $file;
        $content = is_array($content) ? json_encode($content, 256) : $content;
        return File::save($configsDir."/".$file, $content);
    }
}