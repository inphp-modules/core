<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Licensed ( https://opensource.org/licenses/MIT )
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// +----------------------------------------------------------------------
// | 全局错误
// +----------------------------------------------------------------------
namespace Inphp\Core\Util;

class Error
{
    /**
     * 全局错误列表
     * @var array
     */
    public static array $list = [];

    /**
     * 根据错误码，获取错误信息
     * @param int $code
     * @return string
     */
    public static function get(int $code): string
    {
        return self::$list["code_".$code] ?? "未知错误代码：{$code}";
    }

    /**
     * 设置
     * @param array $list
     */
    public static function set(array $list)
    {
        foreach ($list as $key => $value) {
            self::$list["code_{$key}"] = $value;
        }
    }
}