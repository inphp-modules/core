<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Licensed ( https://opensource.org/licenses/MIT )
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// +----------------------------------------------------------------------
// | 数组操作
// +----------------------------------------------------------------------
namespace Inphp\Core\Util;

class Arr
{
    /**
     * 获取数组的键值
     * @param array $array
     * @param string|null $path
     * @param mixed $default
     * @return mixed
     */
    public static function get(array $array, ?string $path = null, mixed $default = null): mixed
    {
        if (null === $path) {
            return $array;
        }
        $path = str_replace("/", ".", $path);
        $keys = explode(".", $path);
        $here = $array;
        foreach ($keys as $key) {
            if (isset($here[$key])) {
                $here = is_object($here[$key]) ? (array)$here[$key] : $here[$key];
            } else {
                $here = null;
                break;
            }
        }
        return $here ?? $default;
    }

    /**
     * 设置或修改数组的某个键值
     * @param array $array
     * @param string|null $path
     * @param mixed $value
     * @return array
     */
    public static function set(array &$array, ?string $path, mixed $value): array
    {
        if (null === $path) {
            $array = $value;
            return $array;
        }
        $path = str_replace("/", ".", $path);
        $keys = explode(".", $path);
        $len = count($keys);
        foreach ($keys as $i => $key) {
            if ($i < $len-1) {
                //非最后一个KEY
                if (!isset($array[$key])) {
                    $array[$key] = [];
                }
            } else {
                //最后一个KEY
                $array[$key] = $value;
            }
        }
        return $array;
    }

    /**
     * 根据条件获取一个列表
     * @param array $array
     * @param array $where
     * @return array
     */
    public static function getList(array $array, array $where): array
    {
        $arr = [];
        foreach ($array as $item) {
            $bool = true;
            foreach ($where as $key => $val) {
                if (isset($item[$key])) {
                    if ($item[$key] != $val) {
                        $bool = false;
                    }
                } else {
                    $bool = false;
                }
            }
            if ($bool) {
                $arr[] = $item;
            }
        }
        return $arr;
    }

    /**
     * 根据条件获取第一行
     * @param array $array
     * @param array|null $where
     * @return mixed
     */
    public static function getFirst(array $array, array|null $where = null): mixed
    {
        if (is_null($where)) {
            return reset($array);
        } else {
            $res = self::getList($array, $where);
            if (!empty($res)) {
                return reset($res);
            }
        }
        return null;
    }

    /**
     * 移除数组的键值！
     * @param array $array
     * @param array|string $keys
     * @return array
     */
    public static function unsetKey(array &$array, array|string $keys): array
    {
        $keys = is_array($keys) ? $keys : [$keys];
        foreach ($keys as $key) {
            unset($array[$key]);
        }
        return $array;
    }

    /**
     * 替换
     * @param array $array
     * @param string $key
     * @param mixed $value
     * @return array
     */
    public static function replace(array $array, string $key, mixed $value): array
    {
        return self::set($array, $key, $value);
    }
}