<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Licensed ( https://opensource.org/licenses/MIT )
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// +----------------------------------------------------------------------
// | 通用连接池
// +----------------------------------------------------------------------
namespace Inphp\Core\Db;

use RuntimeException;
use Swoole\Coroutine\Channel;
use Throwable;

class ConnectionPool
{
    /** @var int 默认连接数量 */
    public const DEFAULT_SIZE = 64;

    /** @var Channel|null 连接池 */
    protected Channel|null $pool = null;

    /** @var callable */
    protected $constructor;

    /** @var int */
    protected int $size = 64;

    /** @var int */
    protected int $num = 0;

    /** @var null|string */
    protected ?string $proxy = null;

    /**
     * 初始化
     * @param callable $constructor
     * @param int $size
     * @param string|null $proxy
     */
    public function __construct(callable $constructor, int $size = self::DEFAULT_SIZE, ?string $proxy = null)
    {
        $this->pool = new Channel($this->size = $size);
        $this->constructor = $constructor;
        $this->num = 0;
        $this->proxy = $proxy;
    }

    /**
     * 自动填充连接池
     * @throws Throwable
     */
    public function fill(): void
    {
        while ($this->size > $this->num) {
            $this->make();
        }
    }

    /**
     * 获取接连对象
     * @throws Throwable
     */
    public function get(float $timeout = -1)
    {
        if ($this->pool === null) {
            throw new RuntimeException('连接池已关闭');
        }
        if ($this->pool->isEmpty() && $this->num < $this->size) {
            $this->make();
        }
        return $this->pool->pop($timeout);
    }

    /**
     * 回收连接对象
     * @throws Throwable
     */
    public function put($connection): void
    {
        if ($this->pool === null) {
            return;
        }
        if ($connection !== null) {
            $this->pool->push($connection);
        } else {
            /* connection broken */
            $this->num -= 1;
            $this->make();
        }
    }

    /**
     * 关闭连接池
     */
    public function close(): void
    {
        $this->pool->close();
        $this->pool = null;
        $this->num = 0;
    }

    /**
     * @throws Throwable
     */
    protected function make(): void
    {
        $this->num++;
        try {
            if ($this->proxy) {
                $connection = new $this->proxy($this->constructor);
            } else {
                $constructor = $this->constructor;
                $connection = $constructor();
            }
        } catch (Throwable $throwable) {
            $this->num--;
            throw $throwable;
        }
        $this->put($connection);
    }
}