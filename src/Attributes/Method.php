<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Licensed ( https://opensource.org/licenses/MIT )
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// +----------------------------------------------------------------------
// | 请求方式过滤
// +----------------------------------------------------------------------
namespace Inphp\Core\Attributes;

#[\Attribute(\Attribute::TARGET_METHOD)] class Method
{
    const POST = "POST";
    const GET = "GET";

    private array $accessMethods = ["*"];

    public function __construct()
    {
        if (func_num_args() === 1) {
            $methods = func_get_args()[0];
            $this->accessMethods = is_array($methods) ? $methods : [$methods];
        } elseif (func_num_args() > 1) {
            $this->accessMethods = func_get_args();
        }
    }

    public function process()
    {
        // TODO: Implement process() method.
        $client = getClient();
        if ($client->method != "OPTIONS" && !in_array("*", $this->accessMethods) && !in_array($client->method, $this->accessMethods)) {
            getHttpResponse()->withJson(httpMessage(405, "Method Not Allowed")->toArray())->end();
        }
    }
}