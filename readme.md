# INPHP 核心库
* [中文文档](https://inphp.cc)

## 支持服务
- [x] HTTP
- [x] Websocket

## 内置操作库
- [x] DB - 支持连接池，PHP原生PDO对象，支持事务，内置语法目前仅适配Mysql！
- [x] Redis - 支持连接池
- [x] Session
- [x] Cookie

## 环境说明
+ PHP >= 8.0
+ 使用了swoole拓展，支持CLI运行服务，开发时基于swoole 5.0
+ 支持PHP-FPM方式运行，此方式不支持使用swoole拓展的相关的类、函数、变量、常量

## 安装方法
composer
```bash
composer require inphp/core
```